from tictactoe import initial_state, player, actions

board = initial_state()

nextPlayer = player(board)

print("player: ", nextPlayer)

allActions = actions(board)

print("actions: ", allActions)