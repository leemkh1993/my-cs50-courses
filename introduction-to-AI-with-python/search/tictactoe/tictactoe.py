"""
Tic Tac Toe Player
"""

import math
import copy

X = "X"
O = "O"
EMPTY = None


def initial_state():
    """
    Returns starting state of the board.
    """
    return [[EMPTY, EMPTY, EMPTY],
            [EMPTY, EMPTY, EMPTY],
            [EMPTY, EMPTY, EMPTY]]


def player(board):
    """
    Returns player who has the next turn on a board.
    """
    xCount = 0
    oCount = 0

    for row in board:
        for cell in row:
            if cell == X:
                xCount += 1
            if cell == O:
                oCount += 1

    if xCount == 0 and oCount == 0:
        return X
    elif xCount == oCount:
        return X
    else:
        return O


def actions(board):
    """
    Returns set of all possible actions (i, j) available on the board.
    """
    newSet = set()
    for rowIndex, row in enumerate(board):
        for cellIndex, cell in enumerate(row):
            if cell is None:
                newSet.add((rowIndex, cellIndex))
    return newSet


def result(board, action):
    """
    Returns the board that results from making move (i, j) on the board.
    """
    newBoard = copy.deepcopy(board)
    (row, cell) = action
    if (newBoard[row][cell] is not None):
        raise Exception

    newBoard[row][cell] = player(newBoard)
    return newBoard


def winner(board):
    """
    Returns the winner of the game, if there is one.
    """

    vertical = [[], [], []]
    diagonal = [[], []]

    for rowIndex, row in enumerate(board):
        if O not in row and None not in row: return X
        if X not in row and None not in row: return O
        for cellIndex, cell in enumerate(row):
            vertical[cellIndex].append(cell)
            
            if (rowIndex == 1 and cellIndex == 1):
                diagonal[0].append(cell)
                diagonal[1].append(cell)

            if ((cellIndex == 0 and rowIndex == 0) or (cellIndex == 2 and rowIndex == 2)):
                diagonal[0].append(cell)

            if ((cellIndex == 2 and rowIndex == 0) or (cellIndex == 0 and rowIndex == 2)):
                diagonal[1].append(cell)
                
    for row in vertical:
        if O not in row and None not in row: return X
        if X not in row and None not in row: return O

    for row in diagonal:
        if O not in row and None not in row: return X
        if X not in row and None not in row: return O


    return None


def terminal(board):
    """
    Returns True if game is over, False otherwise.
    """
    player = winner(board)

    if player is not None:
        return True

    flatBoard = []

    for row in board:
        for cell in row:
            flatBoard.append(cell)

    if player is None and None not in flatBoard:
        return True

    return False


def utility(board):
    """
    Returns 1 if X has won the game, -1 if O has won, 0 otherwise.
    """
    if terminal(board) is False: return

    player = winner(board)

    if player == X:
        return 1

    if player == O:
        return -1

    if player == None:
        return 0


def minimax(board):
    """
    Returns the optimal action for the current player on the board.
    """
    raise NotImplementedError
